import pages from '../../resolvers/pages'
import { Context } from '../../types'
import { Pages } from '../../models'

const mockPageId = '5ea3d37fd95f896d192524f4'
const mockUrl = 'http://placeholder.com/image.jpg'

jest.spyOn(Pages, 'getPage')
  .mockImplementation(() => Promise.resolve({
    pageId: mockPageId,
    url: mockUrl,
  } as Pages.PageType))

const mockContext: Context = {} as Context


describe('Pages resolver', () => {
  it('should return page with given ID', async () => {
    const page = await pages.Query.page({}, { pageId: mockPageId }, mockContext)

    expect(page?.pageId).toEqual(mockPageId)
    expect(page?.url).toEqual(mockUrl)
  })
})


