import { Issue, Page, Context } from '../types'
import { Issues, Pages } from '../models'

type CreateIssueInput = {
  createIssueInput: {
    title: string;
  };
}

const issues = function (_: object, _args: object, context: Context): Promise<Issue[] | null> {
  const { db } = context
  return Issues.issues(db)
}

const createIssue = function(_: object, args: CreateIssueInput, context: Context): Promise<Issue | null> {
  const { db } = context
  const { title } = args?.createIssueInput

  return Issues.createIssue(db, title)
}

const pages =  function (issue: Issue, _: object, context: Context): Promise<Page[] | null> {
  const { db } = context
  const { issueId } = issue
  return Pages.getPages(db, issueId)
}

export default {
  Resolvers: {
    Issue: {
      pages,
    },
  },
  Query: {
    issues,
  },
  Mutation: {
    createIssue,
  },
}
