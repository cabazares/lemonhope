import { v4 as uuid } from 'uuid'
import { Page, Context } from '../types'
import { Pages } from '../models'
import { FileUpload } from 'graphql-upload'
import { uploadToS3FromStream } from '../clients/s3'

type PageInput = {
  pageId: string;
}

type CreatePageInput = {
  createPageInput: {
    image: FileUpload;
    issueId: string;
  };
}

const page = async function (_: object, pageInput: PageInput, context: Context): Promise<Page | null> {
  const { pageId } = pageInput
  const { db } = context
  return await Pages.getPage(db, pageId)
}

const createPage = async function (_: object, args: CreatePageInput, context: Context): Promise<Page | null> {
  const { db } = context
  const { image, issueId } = args?.createPageInput
  const { createReadStream } = await image
  const stream = createReadStream()

  const s3File = await uploadToS3FromStream(stream, uuid())

  return await Pages.createPage(db, issueId, s3File.Location)
}

export default {
  Resolvers: {},
  Query: {
    page,
  },
  Mutation: {
    createPage,
  },
}
