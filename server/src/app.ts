import dotenv from 'dotenv'
dotenv.config()

import express from 'express'
import { GraphQLError } from 'graphql'
import graphqlHTTP from 'express-graphql'
import { makeExecutableSchema } from 'graphql-tools'
import { GraphQLUpload, graphqlUploadExpress } from 'graphql-upload'

import { connect } from './models'
import pages from './resolvers/pages'
import issues from './resolvers/issues'

const typeDefs = `
  scalar Upload

  type Page {
    pageId: ID!
    url: String!
    createdAt: String!
    updatedAt: String!
  }

  type Issue {
    issueId: ID!
    title: String!
    pages: [Page]
  }

  type Query {
    page(pageId: String!): Page
    issues: [Issue]
  }

  input CreatePageInput {
    image: Upload
    issueId: ID
  }

  input CreateIssueInput {
    title: String!
  }

  type Mutation {
    createIssue(createIssueInput: CreateIssueInput!): Issue
    createPage(createPageInput: CreatePageInput!): Page
  }
`

const app = express()

app.use('/graphql',
  graphqlUploadExpress(),
  graphqlHTTP(async () => ({
    schema: makeExecutableSchema({
      typeDefs,
      resolvers: {
        Upload: GraphQLUpload,
        Query: {
          ...pages.Query,
          ...issues.Query,
        },
        Mutation: {
          ...pages.Mutation,
          ...issues.Mutation,
        },
        ...pages.Resolvers,
        ...issues.Resolvers,
      },
    }),
    context: {
      db: await connect(),
    },
    graphiql: true,
    ...(process.env.NODE_ENV !== 'production'? {
      customFormatErrorFn: (error: GraphQLError): object => ({
        message: error.message,
        locations: error.locations,
        stack: error.stack ? error.stack.split('\n') : [],
        path: error.path,
      }),
    } : {}),
  })),
)


const port = process.env.PORT ?? 4000

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))
