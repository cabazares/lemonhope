import mongoose from 'mongoose'

export interface Context {
  db: mongoose.Connection;
}

// ------------------------------

export interface Page {
  pageId: string;
  issueId: string;
  url: string;
  createdAt: string;
  updatedAt: string;
}

export interface Issue {
  issueId: string;
  title: string;
  createdAt: string;
  updatedAt: string;
}
