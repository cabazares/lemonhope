import { S3 } from 'aws-sdk'
import { Readable } from 'stream'

const {
  S3_ACCESS_KEY_ID,
  S3_SECRET_ACCESS_KEY,
  S3_BUCKET,
} = process.env

const s3 = new S3({
  accessKeyId: S3_ACCESS_KEY_ID,
  secretAccessKey: S3_SECRET_ACCESS_KEY,
  region: 'ap-southeast-1',
})

const uploadFromStream = function (s3: S3, stream: Readable, key: string): Promise<S3.ManagedUpload.SendData> {
  return s3.upload({
    ACL: 'public-read',
    Bucket: S3_BUCKET || 'lemonhope',
    Key: `pages/${key}`,
    Body: stream,
  }).promise()
}

export const uploadToS3FromStream = function (readable: Readable, key: string): Promise<S3.ManagedUpload.SendData> {
  return uploadFromStream(s3, readable, key)
}
