import { Connection, Document, Schema, Model} from 'mongoose'
import { Issue } from '../types'

export type IssueType = Issue & Document;

const IssueSchema = new Schema({
  title: String,
}, {
  timestamps: true,
  toJSON: { virtuals: true }
})

IssueSchema.virtual('issueId').get(function(this: Document) {
  return this._id
})

let IssueModel: Model<IssueType>
const getIssueModel = function (db: Connection): Model<IssueType> {
  if (!IssueModel) {
    IssueModel = db.model<IssueType>('Issue', IssueSchema)
  }
  return IssueModel
}

const getIssue = function (db: Connection, issueId: string): Promise<IssueType | null> {
  const Issue = getIssueModel(db)
  return Issue.findById(issueId).exec()
}

const createIssue = async function(db: Connection, title: string): Promise<IssueType | null> {
  const Issue = getIssueModel(db)
  const issue = new Issue({
    title,
  })
  await issue.save()

  return issue
}

const issues = function (db: Connection): Promise<IssueType[] | null> {
  const Issue = getIssueModel(db)

  return Issue.find({}).exec()
}

export {
  issues,
  getIssue,
  createIssue,
}
