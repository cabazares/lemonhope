import mongoose from 'mongoose'
export * as Pages from './pages'
export * as Issues from './issues'

const MONGO_URI = process.env.MONGO_URI ?? 'mongodb://localhost:27017/lemonhope'

const connect = async function (): Promise<mongoose.Connection> {
  await mongoose.connect(MONGO_URI, { useNewUrlParser: true })
  return mongoose.connection
}

export {
  connect,
}
