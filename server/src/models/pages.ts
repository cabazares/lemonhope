import { Connection, Document, Schema, Model} from 'mongoose'
import { Page } from '../types'

export type PageType = Page & Document;

const PageSchema = new Schema({
  url: String,
  issueId: {type: Schema.Types.ObjectId, ref: 'Issue'},
}, {
  timestamps: true,
  toJSON: { virtuals: true }
})

PageSchema.virtual('pageId').get(function(this: Document) {
  return this._id
})

let PageModel: Model<PageType>
const getPageModel = function (db: Connection): Model<PageType> {
  if (!PageModel) {
    PageModel = db.model<PageType>('Page', PageSchema)
  }
  return PageModel
}

const getPage = function (db: Connection, pageId: string): Promise<PageType | null> {
  const Page = getPageModel(db)
  return Page.findById(pageId).exec()
}

const createPage = async function(db: Connection, issueId: string, url: string): Promise<PageType | null> {
  const Page = getPageModel(db)
  const page = new Page({
    issueId,
    url,
  })
  await page.save()

  return page
}

const getPages = function (db: Connection, issueId: string): Promise<PageType[] | null> {
  const Page = getPageModel(db)

  return Page.find({
    issueId,
  }).exec()
}

export {
  getPage,
  getPages,
  createPage,
}
