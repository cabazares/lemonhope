
# Lemonhope


## Development

### Requriments

- node
- docker

### server

`npm run start`

### client

`npm run start`

#### reverse proxy

```
cd clients
./proxy.sh
```

this will start nginx running inside docker as a reverse proxy.
It should display a link to where you can test view the app

```
Starting proxy server: http://192.168.0.3
```

### docker

persist postgres

`docker volume create --name=mongodb_data_volume`

start all containers
`docker-compose up`

