import React from 'react'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import SiteHeader from './components/SiteHeader'
import IssueList from './components/IssueList'
import PageList from './components/PageList'


const ISSUES_QUERY = gql`
  {
    issues {
      issueId
      title
      pages {
        pageId
        url
      }
    }
  }
`

function App() {
  const [issue, setIssue] = React.useState()

  const handleChange = (selectedIssue) => {
    setIssue(selectedIssue);
  }

  return (
    <>
      <SiteHeader />
      <Query
        query={ISSUES_QUERY}
        onCompleted={({ issues }) => setIssue(issues[0])}
      >
        {({ loading, error, data }) => {
          if (loading) return <div>Fetching</div>
          if (error) return <div>Error</div>

          const { issues } = data

          return (
            <div>
              <IssueList issues={issues} onSelect={handleChange} />
              <hr />
              {issue && <PageList issue={issue} />}
            </div>
          )
        }}
      </Query>
    </>
  )
}

export default App
