import React from 'react'
import ReactDOM from 'react-dom'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom"
import CssBaseline from '@material-ui/core/CssBaseline'

import { ApolloProvider } from 'react-apollo'
import { ApolloClient } from 'apollo-client'
import { createUploadLink } from 'apollo-upload-client'
import { InMemoryCache } from 'apollo-cache-inmemory'

import App from './App';
import * as serviceWorker from './serviceWorker';
import IssueReader from './components/IssueReader';


const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: createUploadLink({ uri:'/graphql' }),
})

ReactDOM.render(
  <React.StrictMode>
    <CssBaseline />
    <ApolloProvider client={client}>
      <Router>
        <Switch>
          <Route path="/issue/:issueId" component={IssueReader} />
          <Route exact path="/">
            <App />
          </Route>
        </Switch>
      </Router>
    </ApolloProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
