import React from 'react'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import SiteHeader from './SiteHeader'

import Pagination from '@material-ui/lab/Pagination';

const ISSUE_QUERY = gql`
  {
    issues {
      issueId
      title
      pages {
        pageId
        url
      }
    }
  }
`

const IssueReader = function ({ match }) {
  const { issueId } = match.params

  const [currentPageIndex, setCurrentPageIndex] = React.useState(0)

  return (
    <>
      <SiteHeader />
      <Query
        query={ISSUE_QUERY}
      >
        {({ loading, error, data }) => {
          if (loading) return <div>Fetching</div>
          if (error) return <div>Error</div>

          const { issues } = data
          const [ issue ] = issues.filter(item => item.issueId === issueId)

          if (!issue) {
            return 'Issue not found'
          }

          return (
            <div>
              <img src={issue.pages[currentPageIndex].url} />
              <Pagination count={issue.pages.length} onChange={
                (_, pageIndex) => {
                  setCurrentPageIndex(pageIndex - 1)
                }
              } />
            </div>
          )
        }}
      </Query>
    </>
  )
}

export default IssueReader
