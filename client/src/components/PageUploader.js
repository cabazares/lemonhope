import React from 'react'
import { useMutation } from '@apollo/react-hooks'
import gql from 'graphql-tag'
import {
  Paper,
  Typography,
} from '@material-ui/core'

const MUTATION = gql`
  mutation($file: Upload!, $issueId: ID!) {
    createPage(createPageInput: {
      image: $file
      issueId: $issueId
    }) {
      pageId,
      url,
    }
  }
`

function UploadFile({ issueId }) {
  const [mutate] = useMutation(MUTATION);

  function onChange({
    target: {
      validity,
      files: [file],
    },
  }) {
    if (validity.valid) mutate({ variables: { file, issueId } });
  }

  return (
    <Paper>
      <Typography>Add Page</Typography>
      <input type="file" required onChange={onChange} />
    </Paper>
  )
}

export default UploadFile
