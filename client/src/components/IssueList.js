import React from 'react'
import { useHistory } from "react-router-dom"
import { useMutation } from '@apollo/react-hooks'
import gql from 'graphql-tag'
import { makeStyles } from '@material-ui/core/styles'
import {
  Grid,
  Container,
  Button,
  Card,
  CardContent,
  CardMedia,
  CardActionArea,
  Typography
} from '@material-ui/core'


const useStyles = makeStyles((theme) => ({
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
}))

const MUTATION = gql`
  mutation($title: String!) {
    createIssue(createIssueInput: {
      title: $title
    }) {
      issueId
      title
      pages {
        pageId
        url
      }
    }
  }
`

function IssueList ({ issues, onSelect }) {
  const classes = useStyles()
  const history = useHistory()
  const [mutate] = useMutation(MUTATION)

  const createIssue = async function () {
    const { data } = await mutate({ variables: { title: 'New Issue' }})
    // FIXME: reload data instead of refreshing page
    window.location.reload()
    return data?.createIssue
  }

  return (
    <Container className={classes.cardGrid} maxWidth="md">
      <Grid container spacing={4}>
        {issues.map((issue) => (
          <Grid item key={issue.issueId} xs={12} sm={6} md={4}>
            <Card className={classes.card}>
              <CardActionArea onClick={() => onSelect(issue)}>
                <CardMedia
                  className={classes.cardMedia}
                  image={issue.pages[0]?.url || 'https://source.unsplash.com/random'}
                  title={issue.title}
                />
                <CardContent className={classes.cardContent}>
                  <Typography gutterBottom variant="h5" component="h2">
                    {issue.title}
                  </Typography>
                </CardContent>
              </CardActionArea>
              <Button size="small" color="primary"
                  onClick={() => history.push(`/issue/${issue.issueId}`)}>
                View Issue
              </Button>
            </Card>
          </Grid>
        ))}
          <Grid item xs={12} sm={6} md={4}>
            <Card className={classes.card}>
              <CardActionArea onClick={createIssue}>
                <CardContent className={classes.cardContent}>
                  <Typography gutterBottom variant="h5" component="h2">
                    Create new issue
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
      </Grid>
    </Container>
  )
}

export default IssueList
