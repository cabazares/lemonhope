import React from 'react'
import { render } from '@testing-library/react'
import Page from './page'

const mockPage = {
  pageId: '5ea3d37fd95f896d192524f4',
  url: 'http://placeholder.com/image.jpg',
}

test('renders page image', () => {
  const { getByRole } = render(<Page page={mockPage} />)
  const imgElement = getByRole('img')

  expect(imgElement).toBeInTheDocument()
});
