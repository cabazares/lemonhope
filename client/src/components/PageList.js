import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Page from './page'
import UploadFile from './PageUploader'


const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(2),
      padding: theme.spacing(1),
    },
  },
}))

function PageList({ issue }) {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      {issue && issue.pages.map(page => (
        <Page key={page.pageId} page={page} />
      ))}
      {issue && <UploadFile issueId={issue.issueId} />}
    </div>
  )
}

export default PageList
