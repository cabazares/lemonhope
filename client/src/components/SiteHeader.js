import React from 'react'
import { useHistory } from "react-router-dom"
import {
  Box,
  AppBar,
  Toolbar,
  Typography,
} from '@material-ui/core'
import {
  LibraryBooks as LibraryBooksIcon
} from '@material-ui/icons'
import { makeStyles } from '@material-ui/core/styles'


const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
}))

function SiteHeader() {
  const classes = useStyles()
  const history = useHistory()

  return (
    <AppBar position="relative">
      <Toolbar>
        <Box onClick={() => history.push('/')}>
          <Typography variant="h6" color="inherit" noWrap>
            <LibraryBooksIcon className={classes.icon} color="inherit"/>
            LemonHope
          </Typography>
        </Box>
      </Toolbar>
    </AppBar>
  )
}

export default SiteHeader
