import React from 'react'
import { Paper } from '@material-ui/core'


const Page = function ({ page }) {
  const { url } = page
  return (
    <Paper>
      <img src={url} alt="page" />
    </Paper>
  )
}


export default Page
