#!/usr/bin/env bash

mkdir -p .proxy

NETWORK_IP="$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1')"
sed -e "s/_HOSTNAME_/$NETWORK_IP/g" nginx.conf > .proxy/nginx.conf

echo "Starting proxy server: http://$NETWORK_IP"
# run nginx inside docker
docker run --rm --name dev-nginx -p 80:80 -v $PWD/.proxy/nginx.conf:/etc/nginx/nginx.conf:ro  nginx:1.11.8-alpine
